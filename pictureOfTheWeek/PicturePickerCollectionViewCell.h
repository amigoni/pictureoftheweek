//
//  picturePickerCollectionViewCell.h
//  pictureOfTheWeek
//
//  Created by Leonardo Amigoni on 12/30/15.
//  Copyright © 2015 Mozzarello. All rights reserved.
//

#import <UIKit/UIKit.h>
@import Photos;

@interface PicturePickerCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *bgImage;
@property (strong, nonatomic) PHAsset *cellAssetData;

- (void) configureView: (PHAsset*)assetData;

@end
