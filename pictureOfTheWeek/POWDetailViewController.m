//
//  POWDetailViewController.m
//  pictureOfTheWeek
//
//  Created by Leonardo Amigoni on 12/28/15.
//  Copyright © 2015 Mozzarello. All rights reserved.
//

#import "POWDetailViewController.h"
#import "DateFormatter.h"

@interface POWDetailViewController ()

@end

@implementation POWDetailViewController

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    DateFormatter *dateFormatter;
    dateFormatter = [[DateFormatter alloc] init];
    
    CGFloat scale = [UIScreen mainScreen].scale; //Gets 2x or 3x accordingly
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    CGRect newFrame = self.imageContainer.frame;
    newFrame.size.width = screenWidth;
    newFrame.size.height = screenWidth;
    [self.imageContainer setFrame:newFrame];
    
    [self.actionButton.layer setCornerRadius:10];
    
    
    PHImageManager *photoManager = [[PHImageManager alloc] init];
    PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
    options.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
    options.synchronous = YES;
    options.resizeMode = PHImageRequestOptionsResizeModeExact;
    
    
    //CGSize cellSize = ((UICollectionViewFlowLayout *)self.collectionViewLayout).itemSize;
    CGSize size = CGSizeMake(screenWidth*scale, screenWidth*scale);
    
    [photoManager requestImageForAsset:self.asset targetSize:size contentMode:PHImageContentModeAspectFill options:options resultHandler:^(UIImage *resultImage, NSDictionary *info){
        self.imageView.image = resultImage;
    }];
    
    
    NSInteger *weekNumber = [dateFormatter getWeekNumberString:self.pictureOfTheWeekData.date];
    
    self.topBarLabel.text = [NSString stringWithFormat:@"Week %ld", (long)weekNumber];
    self.weekLabel.text = [NSString stringWithFormat:@"Week %ld", (long)weekNumber];
    
    self.dateLabel.text = [NSString stringWithFormat:@"%@", [dateFormatter getDateRangeString:self.pictureOfTheWeekData.date]];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotification:)
                                                 name:@"CaptionUpdated"
                                               object:nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)receiveNotification :(NSNotification *) notification {
    if ([[notification name] isEqualToString:@"CaptionUpdated"]){
        NSString *text = [notification.userInfo objectForKey:@"text"];
        NSLog (@"%@",text);
        
        if (text.length == 0) {
            text = @"enter caption";
        } else {
            
        }
        [self.captionButton setTitle:text forState:UIControlStateNormal];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)onActionButtonPressed:(id)sender {
    
    NSError *error = nil;
    
    self.pictureOfTheWeekData.pictureID = self.asset.localIdentifier;
    if (self.captionButton.titleLabel.text.length > 0){
        self.pictureOfTheWeekData.title = self.captionButton.titleLabel.text;
        NSDictionary *userInfo = [NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"%@",self.pictureOfTheWeekData.pictureID] forKey:@"pictureID"];
        [[NSNotificationCenter defaultCenter] postNotificationName: @"POWUpdated" object:nil userInfo:userInfo];
    }
    
    if (![self.pictureOfTheWeekData.managedObjectContext save:&error]) {
        NSLog(@"Unable to save managed object context.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
    
}

- (IBAction)onCloseButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onCaptionButtonPressed:(id)sender {
    
}

@end
