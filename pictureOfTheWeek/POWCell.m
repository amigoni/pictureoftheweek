//
//  POWCell.m
//  pictureOfTheWeek
//
//  Created by Leonardo Amigoni on 12/28/15.
//  Copyright © 2015 Mozzarello. All rights reserved.
//

#import "POWCell.h"
@import Photos;

@implementation POWCell

DateFormatter *dateFormatter;

- (void)awakeFromNib {
    // Initialization code
    dateFormatter = [[DateFormatter alloc] init];
    
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureCell: (PictureOfTheWeek*) pictureOfTheWeekData {
    self.pictureOfTheWeekData = pictureOfTheWeekData;
    
    if(pictureOfTheWeekData.pictureID == nil){
        self.bgImage.hidden = true;
        self.statusContainerView.hidden = false;
    } else {
        NSArray *localIdentifiers = [NSArray arrayWithObjects:pictureOfTheWeekData.pictureID, nil];
        PHFetchResult *photos = [PHAsset fetchAssetsWithLocalIdentifiers: localIdentifiers options:nil];
        
        CGFloat scale = [UIScreen mainScreen].scale; //Gets 2x or 3x accordingly
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth = screenRect.size.width;
        
        PHImageManager *photoManager = [[PHImageManager alloc] init];
        PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
        options.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
        options.synchronous = YES;
        options.resizeMode = PHImageRequestOptionsResizeModeExact;
        CGSize size = CGSizeMake(200*scale, screenWidth*scale);
        
        //Work the case that the photo doesn't exist anymore
        if (photos[0]) {
            self.asset = photos[0];
            [photoManager requestImageForAsset:photos[0] targetSize:size contentMode:PHImageContentModeAspectFill options:options resultHandler:^(UIImage *resultImage, NSDictionary *info){
                self.bgImage.image = resultImage;
                self.bgImage.hidden = false;
                self.statusContainerView.hidden = true;
            }];
        }
        
    }
    
    self.titleLabel.text = pictureOfTheWeekData.title;
    
    self.weekLabel.text = [NSString stringWithFormat:@"Week %ld", (long)[dateFormatter getWeekNumberString:pictureOfTheWeekData.date]];
    
    self.dateLabel.text = [NSString stringWithFormat:@"%@", [dateFormatter getDateRangeString:pictureOfTheWeekData.date]];
}

@end
