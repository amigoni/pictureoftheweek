//
//  POWList.m
//  pictureOfTheWeek
//
//  Created by Leonardo Amigoni on 12/28/15.
//  Copyright © 2015 Mozzarello. All rights reserved.
//

#import "POWListViewController.h"
#import "POWCell.h"
#import "StatusCell.h"
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
@import Photos;

@implementation POWListViewController

{
    NSArray *tableData;
    AppDelegate *appDelegate;
}


- (void)viewDidLoad{
    [super viewDidLoad];
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self fetchData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotification:)
                                                 name:@"POWUpdated"
                                               object:nil];
}

-(BOOL)prefersStatusBarHidden {
    return YES;
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:@"PicturePickerSegue"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        PicturePickerViewController *destViewController = segue.destinationViewController;
        destViewController.pictureOfTheWeekData = [tableData objectAtIndex:indexPath.row];
        
        NSDate *startDate = destViewController.pictureOfTheWeekData.date;
        NSDate *endDate = [startDate dateByAddingTimeInterval:(60*60*24*7)];
        
        // Create a PHFetchResult object for each section in the table view.
        PHFetchOptions *photosFetchOptions = [[PHFetchOptions alloc] init];
        photosFetchOptions.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
        
        
        NSPredicate *predicateMediaType = [NSPredicate predicateWithFormat:@"mediaType = %d",PHAssetMediaTypeImage];
        NSPredicate *predicateStartDate = [NSPredicate predicateWithFormat:@"creationDate >= %@", startDate];
        NSPredicate *predicateEndDate = [NSPredicate predicateWithFormat:@"creationDate <= %@", endDate];
        NSCompoundPredicate *compoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicateStartDate,predicateEndDate, predicateMediaType]];
        
        photosFetchOptions.predicate = compoundPredicate;
        
        PHFetchResult *allPhotos = [PHAsset fetchAssetsWithOptions:photosFetchOptions];
        
        NSLog([NSString stringWithFormat:@"Fetched Photos %lu",(unsigned long)allPhotos.count]);
        
        destViewController.assets = allPhotos;

    }
    else if ([segue.identifier isEqualToString:@"POWDetailSegue"]){
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        POWDetailViewController *destViewController = segue.destinationViewController;
        destViewController.pictureOfTheWeekData = [tableData objectAtIndex:indexPath.row];
        
        NSArray *localIdentifiers = [NSArray arrayWithObjects:destViewController.pictureOfTheWeekData.pictureID, nil];
        PHFetchResult *photos = [PHAsset fetchAssetsWithLocalIdentifiers: localIdentifiers options:nil];
        destViewController.asset = photos[0];
    }
}


#pragma mark Tableview

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return tableData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    POWCell *cell = [tableView dequeueReusableCellWithIdentifier:@"POWCell"];
    [cell configureCell: tableData[indexPath.row]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    POWCell *cell = (POWCell*)[tableView cellForRowAtIndexPath:indexPath];
    
    if (cell.pictureOfTheWeekData.pictureID) {
        [self performSegueWithIdentifier:@"POWDetailSegue" sender:cell];
        
    } else {
        [self performSegueWithIdentifier:@"PicturePickerSegue" sender:cell];
    }
}

#pragma mark other function

- (void)receiveNotification :(NSNotification *) notification {
    [self fetchData];
    
    if ([[notification name] isEqualToString:@"POWUpdated"]){
        NSString *pictureID = [notification.userInfo objectForKey:@"pictureID"];
        NSLog (@"Updating TableView: Received Picture ID %@",pictureID);
        
        for (NSInteger j = 0; j < [self.tableView numberOfSections]; j++)
        {
            for (NSInteger i = 0; i < [self.tableView numberOfRowsInSection:j]; ++i)
            {
                POWCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:j]];
                if (cell && [cell.pictureOfTheWeekData.pictureID isEqualToString: pictureID]) {
                    [cell configureCell:tableData[i]];
                    break;
                }                
            }
        }
    }
}

- (void)fetchData{
    //Fetch POW Data
    NSDate *now = [NSDate date];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:NO];
    [request setSortDescriptors:@[sortDescriptor]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"date < %@", now];
    [request setPredicate:predicate];
    
    NSEntityDescription *powEntityDescription = [NSEntityDescription entityForName:@"PictureOfTheWeek" inManagedObjectContext:appDelegate.managedObjectContext];
    [request setEntity:powEntityDescription];
    
    NSError *fetchError = nil;
    tableData = [appDelegate.managedObjectContext executeFetchRequest:request error:&fetchError];
    NSLog(@"TableData: %lu",(unsigned long)tableData.count);
}

@end
