//
//  DateFormatter.h
//  pictureOfTheWeek
//
//  Created by Leonardo Amigoni on 12/30/15.
//  Copyright © 2015 Mozzarello. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateFormatter : NSObject {
    NSCalendar *calendar;
}

@property (nonatomic,retain) NSCalendar *calendar;

+(DateFormatter*) sharedManager;

-(NSInteger *)getWeekNumberString: (NSDate*)date;
-(NSString*)getDateRangeString: (NSDate*) date;

@end
