//
//  DateFormatter.m
//  pictureOfTheWeek
//
//  Created by Leonardo Amigoni on 12/30/15.
//  Copyright © 2015 Mozzarello. All rights reserved.
//

#import "DateFormatter.h"

@implementation DateFormatter

@synthesize calendar;

+ (DateFormatter*)sharedManager
{
    static DateFormatter *sharedMyManager = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        sharedMyManager = [[DateFormatter alloc] init];
    });
    return sharedMyManager;
}

- (id)init {
    if (self = [super init]) {
        calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        [calendar setFirstWeekday:2];
    }
    return self;
}

- (void)dealloc {
    // Should never be called, but just here for clarity really.
}

- (NSInteger*) getWeekNumberString:(NSDate*) date{

    NSDateComponents *dateComponentStartDate = [calendar components:(NSCalendarUnitWeekOfYear | NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYearForWeekOfYear) fromDate: date];

    return dateComponentStartDate.weekOfYear;
}

- (NSString*)getDateRangeString:(NSDate*) date {
    
    NSDateFormatter *formatterStart = [[NSDateFormatter alloc] init];
    [formatterStart setDateFormat:@"MMM d"];
    NSString *startString = [formatterStart stringFromDate:date];
    
    
    NSDate *endDate = [date dateByAddingTimeInterval:(60*60*24*6)];
    
    NSDateFormatter *formatterEnd = [[NSDateFormatter alloc] init];
    [formatterEnd setDateFormat:@"MMM d \u2019YY"];
    NSString *endString = [formatterEnd stringFromDate:endDate];
    
    return [NSString stringWithFormat:@"%@ - %@",startString, endString];
}


@end
