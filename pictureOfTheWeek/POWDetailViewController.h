//
//  POWDetailViewController.h
//  pictureOfTheWeek
//
//  Created by Leonardo Amigoni on 12/28/15.
//  Copyright © 2015 Mozzarello. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PictureOfTheWeek.h"
@import Photos;

@interface POWDetailViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *topBarLabel;
@property (weak, nonatomic) IBOutlet UIView *imageContainer;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *captionButton;

@property (weak, nonatomic) IBOutlet UILabel *weekLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIButton *actionButton;

@property (strong, nonatomic) PictureOfTheWeek *pictureOfTheWeekData;
@property (strong, nonatomic) PHAsset *asset;

@end
