//
//  picturePickerViewController.h
//  pictureOfTheWeek
//
//  Created by Leonardo Amigoni on 12/28/15.
//  Copyright © 2015 Mozzarello. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PictureOfTheWeek.h"
#import "PicturePickerCollectionViewCell.h"
#import "DateFormatter.h"
@import Photos;

@interface PicturePickerViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (nonatomic, strong) PictureOfTheWeek *pictureOfTheWeekData;
@property (nonatomic, strong) PHFetchResult *assets;

@end
