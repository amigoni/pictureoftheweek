//
//  PictureOfTheWeek+CoreDataProperties.h
//  pictureOfTheWeek
//
//  Created by Leonardo Amigoni on 12/28/15.
//  Copyright © 2015 Mozzarello. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "PictureOfTheWeek.h"

NS_ASSUME_NONNULL_BEGIN

@interface PictureOfTheWeek (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *title;
@property (nullable, nonatomic, retain) NSString *pictureID;
@property (nullable, nonatomic, retain) NSNumber *isHidden;
@property (nullable, nonatomic, retain) NSDate *date;

@end

NS_ASSUME_NONNULL_END
