//
//  POWCell.h
//  pictureOfTheWeek
//
//  Created by Leonardo Amigoni on 12/28/15.
//  Copyright © 2015 Mozzarello. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PictureOfTheWeek.h"
#import "DateFormatter.h"
@import Photos;

@interface POWCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *statusContainerView;
@property (weak, nonatomic) IBOutlet UIImageView *bgImage;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *weekLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@property (strong, nonatomic) PictureOfTheWeek *pictureOfTheWeekData;
@property (strong, nonatomic) PHAsset *asset;

- (void) configureCell: (PictureOfTheWeek*)pictureOfTheWeek;

@end
