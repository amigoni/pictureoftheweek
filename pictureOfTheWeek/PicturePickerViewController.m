//
//  picturePickerViewController.m
//  pictureOfTheWeek
//
//  Created by Leonardo Amigoni on 12/28/15.
//  Copyright © 2015 Mozzarello. All rights reserved.
//

#import "PicturePickerViewController.h"
#import "PicturePickerCollectionViewCell.h"
#import "POWDetailViewController.h"

@interface PicturePickerViewController ()

@end

@implementation PicturePickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    // Do any additional setup after loading the view.
    DateFormatter *dateFormatter = [[DateFormatter alloc] init];
    
    NSDate *date = self.pictureOfTheWeekData.date;
    self.subtitleLabel.text = [NSString stringWithFormat:@"Week %ld (%@)",(long)[dateFormatter getWeekNumberString: date],[dateFormatter getDateRangeString: date]
    ];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)prefersStatusBarHidden {
    return YES;
}

- (IBAction)onBackButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - CollectionView

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.assets.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PicturePickerCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"picturePickerCollectionViewCell" forIndexPath:indexPath];
    
    [cell configureView: self.assets[indexPath.row]];
    
    return cell;
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.destinationViewController isKindOfClass:[POWDetailViewController class]])
    {
        POWDetailViewController *destinationVC = [segue destinationViewController];
        NSIndexPath *indexPath = [self.collectionView indexPathForCell:sender];
        destinationVC.pictureOfTheWeekData = self.pictureOfTheWeekData;
        destinationVC.asset = self.assets[indexPath.row];
    }
}


@end
