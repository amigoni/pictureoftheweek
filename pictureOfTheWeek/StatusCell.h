//
//  statusCell.h
//  pictureOfTheWeek
//
//  Created by Leonardo Amigoni on 12/28/15.
//  Copyright © 2015 Mozzarello. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PictureOfTheWeek.h"
#import "DateFormatter.h"

@interface StatusCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *mainLabel;
@property (weak, nonatomic) IBOutlet UIImageView *cameraIcon;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *weekLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

- (void) configureCell: (PictureOfTheWeek *)pictureOfTheWeekData mainLabelText:(NSString *)mainLabelText hideCamera:(BOOL)hideCamera;

@end
