//
//  statusCell.m
//  pictureOfTheWeek
//
//  Created by Leonardo Amigoni on 12/28/15.
//  Copyright © 2015 Mozzarello. All rights reserved.
//

#import "StatusCell.h"

@implementation StatusCell


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)configureCell:(PictureOfTheWeek *)pictureOfTheWeekData mainLabelText:(NSString *)mainLabelText hideCamera:(BOOL)hideCamera{
    DateFormatter *dateFormatter = [[DateFormatter alloc] init];
    
    self.mainLabel.text = mainLabelText;
    self.cameraIcon.hidden = hideCamera;
    
    //self.titleLabel.text = pictureOfTheWeekData.title;
    
    self.weekLabel.text = [NSString stringWithFormat:@"Week %ld", (long)[dateFormatter getWeekNumberString:pictureOfTheWeekData.date]];
    
    self.dateLabel.text = [NSString stringWithFormat:@"%@", [dateFormatter getDateRangeString:pictureOfTheWeekData.date]];
}


@end
