//
//  picturePickerCollectionViewCell.m
//  pictureOfTheWeek
//
//  Created by Leonardo Amigoni on 12/30/15.
//  Copyright © 2015 Mozzarello. All rights reserved.
//

#import "PicturePickerCollectionViewCell.h"

@implementation PicturePickerCollectionViewCell

-(void)configureView:(PHAsset *)assetData{

    self.cellAssetData = assetData;
    
    PHImageManager *photoManager = [[PHImageManager alloc] init];
    PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
    //options.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
    options.synchronous = YES;
    options.resizeMode = PHImageRequestOptionsResizeModeExact;
    
    CGFloat scale = [UIScreen mainScreen].scale; //Gets 2x or 3x accordingly
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;

    //CGSize cellSize = ((UICollectionViewFlowLayout *)self.collectionViewLayout).itemSize;
    CGSize size = CGSizeMake(78.0*scale, 78.0*scale);
    
    [photoManager requestImageForAsset:assetData targetSize:size contentMode:PHImageContentModeAspectFill options:options resultHandler:^(UIImage *resultImage, NSDictionary *info){
        self.bgImage.image = resultImage;
    }];
}

@end
