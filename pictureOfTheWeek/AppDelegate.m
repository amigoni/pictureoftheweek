//
//  AppDelegate.m
//  pictureOfTheWeek
//
//  Created by Leonardo Amigoni on 12/27/15.
//  Copyright © 2015 Mozzarello. All rights reserved.
//

#import "AppDelegate.h"
#import "PictureOfTheWeek.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    DateFormatter *dateFormatter = [[DateFormatter alloc] init];
    
    //Setting the calendar
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM dd, yyyy HH:mm"];
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    [gregorian setFirstWeekday:2];
    
    // Initialize Data
     //1. look at how many POW there are
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *powEntityDescription = [NSEntityDescription entityForName:@"PictureOfTheWeek" inManagedObjectContext:self.managedObjectContext];
    [request setEntity:powEntityDescription];
    
    NSError *fetchError = nil;
    NSArray *coreDataFetchResult = [self.managedObjectContext executeFetchRequest:request error:&fetchError];
    
    
    if (fetchError) {
        NSLog(@"Unable to execute Boot Fetch Request");
        NSLog(@"%@, %@", fetchError, fetchError.localizedDescription);
    } else {
        NSLog(@"Initializing POWs");
        if (coreDataFetchResult.count == 0) {
            //2. if first time boot Create all the POW entities starting from Jan 1st 2000 to dec 31st 2100
            
            NSDateComponents *startDateComponents = [[NSDateComponents alloc] init];
            [startDateComponents setDay:3];
            [startDateComponents setMonth:1];
            [startDateComponents setYear:2000];
            [startDateComponents setHour:0];
            [startDateComponents setMinute:0];
            [startDateComponents setSecond:1];
            [startDateComponents setNanosecond:0];
            NSDate *startDate = [gregorian dateFromComponents:startDateComponents];
            
            NSDateComponents *endDateComponents = [[NSDateComponents alloc] init];
            [endDateComponents setDay:31];
            [endDateComponents setMonth:12];
            [endDateComponents setYear:2025];
            [endDateComponents setHour:23];
            [endDateComponents setMinute:59];
            [endDateComponents setSecond:59];
            [endDateComponents setNanosecond:0];
            NSDate *endDate = [gregorian dateFromComponents:endDateComponents];
            
            NSDate *date = startDate;
            
            NSComparisonResult compareResult = [date compare:endDate];
            
            while (compareResult == NSOrderedAscending) {
                date = [date dateByAddingTimeInterval:(60*60*24*7)];
                NSLog(@"Date %@",[formatter stringFromDate:date]);
                
                compareResult = [date compare:endDate];
                
                PictureOfTheWeek *newPOW = [[PictureOfTheWeek alloc] initWithEntity:powEntityDescription insertIntoManagedObjectContext:self.managedObjectContext];
                
                newPOW.date = date;
            }
            
            NSError *saveError = nil;
            
            if(![self.managedObjectContext save:&saveError]){
                NSLog(@"Unable to execute Save Request");
                NSLog(@"%@, %@", saveError, saveError.localizedDescription);
            } else {
                NSLog(@"Successfully Initiated POWs");
            }
        } else {
            NSLog(@"There are %lu POWs",(unsigned long)coreDataFetchResult.count);
        }
    }
    

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.mozzarello.pictureOfTheWeek" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"pictureOfTheWeek" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"pictureOfTheWeek.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

@end
