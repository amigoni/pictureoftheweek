//
//  PictureOfTheWeek+CoreDataProperties.m
//  pictureOfTheWeek
//
//  Created by Leonardo Amigoni on 12/28/15.
//  Copyright © 2015 Mozzarello. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "PictureOfTheWeek+CoreDataProperties.h"

@implementation PictureOfTheWeek (CoreDataProperties)

@dynamic date;
@dynamic title;
@dynamic pictureID;
@dynamic isHidden;

/*
 Notes:
 -date It's important to have a date of when the week starts. The week in theory should start just past Sunday midnight so Monday morning. Problem is what is a person changes timezone? It wouldn't be practical to just use GMT. So we would just need to grab it on a moment to moment basis.
 
 What if we store the date at gmt and the adjust accordingly when you need to pick the picture? So we create all the PictureOfTheWeek
 
 */

@end
